<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->helper('url');
		$this->load->model('welcome_model');
    }
	
	public function index()
	{
		$this->load->view('welcome');

	}
	
	public function ajax_checkuser() {
        $username = $this->input->post('data', TRUE);
        $login_result = $this->welcome_model->check_user($username);
        $msg = '';
        if (count($login_result) > 0) {
            $msg = "The Username Is Already exits!!!";
        }
        echo $msg;
    }
	
	public function register()
	{
	  $this->load->library('form_validation');
	  $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[customers.loginid]');	
	  $this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[customers.email]');
	  $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[confirmpassword]');
	  $this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'trim|required');
	  
	    if ($this->form_validation->run() == FALSE)
		{
		  // Form didn't validate
		  $this->index();
		}
		else
		{
			$now           = date('Y-m-d H:i:s');
			$loginid       = $this->input->post('username');
			$email         = $this->input->post('email');
			$password      = $this->input->post('password');
			$status        = 0;
			
			$data = array(
                   'loginid'   => $loginid,
                   'email'     => $email,
				   'password'  => $password,
				   'status'    => $status,
				   'addeddate' => $now);
				   
		    $id = $this->welcome_model->insert($data);
			if($id > 0){
				$this->session->set_flashdata('flash_message', 'You Succfully Registered..Wiat For Admin To Apporved You!.');
				redirect('Welcome');
				
			}
			
		}
		
	}	
}
