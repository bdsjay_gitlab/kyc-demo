<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
	    parent::__construct();
	    $this->load->helper('url');
	    if ($this->session->userdata('logged_in') == FALSE)
		{
		    redirect('admin/login');
		}
		else
		{
			$this->SessionData = $this->session->userdata('logged_in');
		}	
	}
	
	public function index()
	{
		$this->load->helper('url');
		$this->load->view('admin/welcome');
	}
	
	function logout()
	{
	   $this->load->helper('url');
	   $this->session->unset_userdata('logged_in');
	   //session_destroy();
	   redirect('admin/login');
	}
}