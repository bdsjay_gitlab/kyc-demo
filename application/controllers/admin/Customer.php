<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->table = 'customers';
        $this->uniqueid = 'id';
		$this->load->helper("url");
        if ($this->session->userdata('logged_in') == FALSE) {
            redirect('admin/login');
        } else {
            $this->SessionData = $this->session->userdata('logged_in');
            $this->load->model('admin/customer_model');
            
        }
    }

    public function index() {
        $this->load->view('admin/customer/index');
    }

    public function create() {
        $this->load->view('admin/customer/create');
    }

    public function ajax_getstate() {
        $country_id = $this->input->post('data', TRUE);
        $data['result'] = $this->customer_model->get_state_byid($country_id);
        echo json_encode($data);
    }

    
    public function view($id) {
        $data['username'] = $this->SessionData['username'];
        $data['customer'] = $this->customer_model->getcustomerdetails($id);
        $this->load->view('admin/customer/view', $data);
    }

    public function edit($id) {
        $data['username'] = $this->SessionData['username'];
        
        $data['customer'] = $this->customer_model->getcustomerdetails($id);
        $data['country'] = $this->customer_model->get_country();
        $whercond='';
        if(!empty($data['customer']->country)){
         $whercond = "and cid='".$data['customer']->country."'";  
        }
        $data['state'] = $this->customer_model->get_state_byid($whercond);
        $whercond='';
        if(!empty($data['customer']->state)){
         $whercond = "and sid='".$data['customer']->state."'";  
        }
        $this->load->view('admin/customer/edit', $data);
    }

    public function update($id) {
        $data['username'] = $this->SessionData['username'];
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('mobile', 'Mobile No', 'trim|required');
        $get_uniquid = $this->customer_model->get_value('customers', 'loginid', 'id=' . $id);
        if (trim($get_uniquid->loginid) != trim($this->input->post('username'))) {
            $this->form_validation->set_rules('username', 'Login-ID', 'trim|required|is_unique[customers.loginid]');
        } else {
            $this->form_validation->set_rules('username', 'Login-ID', 'trim|required');
        }
		
		$get_uniqueemail = $this->customer_model->get_value('customers', 'email', 'id=' . $id);
        if (trim($get_uniqueemail->email) != trim($this->input->post('email'))) {
            $this->form_validation->set_rules('email', 'Email-ID', 'trim|required|is_unique[customers.email]');
        } else {
            $this->form_validation->set_rules('email', 'Email-ID', 'trim|required');
        }
		
		$PanImg    = $_POST['panimage_old']; 
	    if(isset($_FILES['panimage']['tmp_name']) && $_FILES['panimage']['size'] > 0){
		$panimage_old = $PanImg;
		$config['upload_path'] = './images/doc/';
		$config['overwrite'] = FALSE;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '2048';
		$IMG1 = time().$_FILES["panimage"]['name'];
		$config['file_name'] = $IMG1;
		$PanImg = $config['file_name'];
		$this->load->library('upload', $config);	
		$this->upload->initialize($config);	
		
		if (!$this->upload->do_upload('panimage')) {
		   
			if ($this->form_validation->run() == FALSE)
			{
			$this->edit($id);
			}
			
			} else {
				
				if (file_exists("./images/doc/".$panimage_old.""))
				{
				$this->load->helper("file");
				unlink("./images/doc/".$panimage_old."");
                
				} 
			}
        }
		
	$AdharImg    = $_POST['adharimage_old'];
	if(isset($_FILES['adharimage']['name']) && $_FILES['adharimage']['size'] > 0){
		$adharimage_old = $AdharImg;
		$config['upload_path'] = './images/doc/';
		$config['overwrite'] = FALSE;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '2048';
		$IMG2 = time().$_FILES["adharimage"]['name'];
		$config['file_name'] = $IMG2;
		$AdharImg = $config['file_name'];	
		$this->load->library('upload', $config);	
		$this->upload->initialize($config);	
		
		if (!$this->upload->do_upload('adharimage')) {
		   
			if ($this->form_validation->run() == FALSE)
			{
			$this->edit($id);
			}
			
			} else {
				
				if (file_exists("./images/doc/".$adharimage_old.""))
				{
				 $path = 'images/doc/'.$adharimage_old;	
				 unlink($path);
                 //$this->load->helper("file");				 
				 //delete_files("./images/doc/".$panimage_old);
				
				} 
			}
        }
		
        if ($this->form_validation->run() == FALSE) {
            $this->edit($id);
        } else {
			
			$data = array(
                   'loginid'          => $this->input->post('username'),
                   'email'            => $this->input->post('email'));
				     
            $Reusltid = $this->customer_model->update($this->table,$this->uniqueid,$id,$data);
			
			if($Reusltid){
			$data = array(
                   'firstname'   => $this->input->post('firstname'),
                   'lastname'    => $this->input->post('lastname'),
				   'mobile'      => $this->input->post('mobile'),
				   'address1'    => $this->input->post('address1'),
				   'address2'    => $this->input->post('address2'),
				   'country'     => $this->input->post('country'),
				   'state'       => $this->input->post('state_id'),
				   'city'        => $this->input->post('city'),
                   'pincode'     => $this->input->post('pincode'),
				   'pancard'     => $PanImg,
				   'adharcard'   => $AdharImg );
				   
            $Reulst = $this->customer_model->update('customer_details','customerid',$id,$data);	
				
			}
			
            $this->session->set_flashdata('flash_message', 'Successfully Update  Customer!.');
            redirect('admin/customer');
        }
    }

    public function delete($id) {
        
        $data['customer'] = $this->customer_model->getcustomerdetails($id);
        $this->load->view('admin/customer/delete', $data);
    }

    public function remove($id) {
        
		$PanImg= $this->customer_model->get_value('customer_details','pancard','customerid='.$id);
        $Path = "./images/doc/".$PanImg->pancard."";
        if (file_exists($Path)) {
            unlink($Path);
        }
		
        $AdharImg= $this->common_model->get_value('customer_details','iadharcard','customerid='.$id);
        $AdharPath = "./images/doc/".$AdharImg->adharcard."";
        if (file_exists($AdharPath)) {
            unlink($AdharPath);
        }
		
        $this->customer_model->delete($this->table, $this->uniqueid, $id);
        $this->customer_model->delete('customer_details','customerid', $id);
		echo "Successfully Deleted Customer.";
        exit;
		}

 
    public function DataTableRefresh() {
		
        $aColumns = array('a.id','a.id', 'a.loginid', 'b.avtar', 'b.firstname','a.email', 'b.mobile','a.status', 'a.id');

        $DataTableArray = getUserDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];
		
		$SearchBy = (isset($_GET['SearchBy']) ? $_GET['SearchBy'] :'');
        if($SearchBy !="")
         {    
           if($SearchBy ==1){
               $Coumn_Name = 'b.firstname';
           }
           else if($SearchBy ==2){
               $Coumn_Name = 'a.email';
			   
           }else if($SearchBy ==3){
               $Coumn_Name = 'b.mobile';
           }
        }
		
		$status = (isset($_GET['status']) ? $_GET['status'] :'');
        if($status !="")
        {            
           if($sWhere<>''){
            $sWhere .= " AND a.status  = '".$status."'"; 
           }else{
               $sWhere .= " WHERE a.status  = '".$status."'"; 
           }                                    
        }
	   
	    $inputkeyword = (isset($_GET['input-keyword']) ? $_GET['input-keyword'] :'');
        if($inputkeyword !=""){
			if($sWhere<>''){
				 $sWhere .= " AND ".$Coumn_Name ." LIKE '$inputkeyword%' "; 
			}else{
				$sWhere .= " WHERE ".$Coumn_Name ." LIKE '$inputkeyword%' "; 
			}    
        }

        
        $DataTableArray = $this->customer_model->LoadOrderDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('checkbox', 'id', 'loginid', 'avtar','name', 'email', 'mobile', 'status', 'Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);

            for ($i = 0; $i < $TotalHeader; $i++) {

                if ($aHeader[$i] == "status") {
                    if ($aRow['status'] == 1) {
                        $status = '<td class="center"><span class="label-success label label-default">Active</span></td>';
                    } else {
                        $status = '<td class="center"><span class="label-default label label-danger">InActive</span></td>';
                    }
                    $row[] = $status;
                } else if ($aHeader[$i] == "avtar") {
					if ($aRow['avtar'] != '') {
                        $avtar = '<td class="center"><a class="magnific" href="'.$site_url.'images/profile/'.$aRow['avtar'].'"><img width="20px" height="20px" src="'.$site_url.'images/profile/'.$aRow['avtar'].'" /></a></td>';
                    } else {
                        $avtar = '<td class="center"><a href="'.$site_url.'images/profile/images.png"><img width="20px" height="20px" src="'.$site_url.'images/profile/images.png" /></a></td>';
                    }
					
					$row[] = $avtar;
				}else if ($aHeader[$i] == "checkbox") {
                    $row[] = '<input type="checkbox" name="id[]" value="'.$aRow['id'].'" >';
					
                } else if ($aHeader[$i] == "Actions") {
                    $Action = '<a style="padding: 0 4px;" class="pull-left" href="'.$site_url.'admin/customer/view/'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="View"><i class="fa fa-eye" style="font-size:18px;color:#337ab7;"></i></a>&nbsp;&nbsp;&nbsp;<a style="padding: 0 4px;" class="pull-left" href="'.$site_url . 'admin/customer/edit/'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil-square-o" style="font-size:18px;color:#337ab7;"></i></a>&nbsp;&nbsp;&nbsp;<a style="padding: 0 4px;" class="pull-left" href="javascript:void(0);" onclick="LoadDeleteDialog(\''.$aRow['name'].'\',\''.$aRow['id'].'\');" rel="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-trash-o" style="font-size:18px;color:#337ab7;"></i></a>';
                    $row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }

    public function Actionongrid($Action) {
        $customerid = $this->input->post('id');
		
        $alert = '';
        if ($Action == 1) {
            foreach ($customerid as $id) {
				 $data = array(
                    'status' => 1);
                $this->customer_model->update('customers','id',$id,$data);
            }
            $alert = 'You successfully Active the Customer(s).';
        } else if ($Action == 2) {
            foreach ($customerid as $id) {
                $data = array(
                    'status' => 0);
                $this->customer_model->update($this->table, 'id', $id, $data);
            }
            $alert = 'You successfully Inactive the Customer(s).';
        } else if ($Action == 3) {
			$alert ='';
            $SuccessFlag=false;
            foreach ($customerid as $id) {
				
				$DeleteFlag = $this->supplier_model->CheckSupplier($id);
				
                    $this->customer_model->delete($this->table,$this->uniqueid, $id);
                    $this->customer_model->delete('customer_details','customerid', $id);
                    $SuccesFlag=true;
                
            }
            
            if($SuccessFlag){
                $alert .= 'You successfully Deleted Customer(s).';
            }
        }
        echo $alert;
    }

}