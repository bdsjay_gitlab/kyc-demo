<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	   function __construct(){
		parent::__construct(); 
        if ($this->session->userdata('logged_in') == TRUE)
		{
			redirect('admin/welcome');
		}
	}
		
	public function index($error='')
	{   
		$data['error'] = $error;
		$this->load->helper('url');
		$this->load->view('admin/login',$data);
	}
	
	public function process(){

	   $this->load->helper('url');
		// Load the model
		$this->load->model('admin/login_model');
		// Validate the user can login
		$result = $this->login_model->validate();
		// Now we verify the result
		if(!$result){
			// If user did not validate, then show them login page again
			$error_message = '<font color=red>Invalid username and/or password.</font><br />';
			$this->index($error_message);
		}else{
			// If user did validate,Send them to members area
			redirect('admin/welcome');
		}		
	}
}