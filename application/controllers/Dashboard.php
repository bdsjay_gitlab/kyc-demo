<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
		
        parent::__construct();
        $this->load->helper('url');
		if ($this->session->userdata('logged') == FALSE) {
            redirect('login');
        } else {
            $this->SessionData = $this->session->userdata('logged');
            $this->load->model('dashboard_model');
			$this->load->library('form_validation');
        }
    }
	
	public function index()
	{
		$data['country'] = $this->dashboard_model->get_country();
        $data['loginid'] = $this->SessionData['loginid'];
		$data['customer'] = $this->dashboard_model->getcustomerdetails($this->SessionData['id']);
		$this->load->view('dashboard',$data);
	}
	

	public function insert()
	{
		var_dump($_FILES['pancard']['name']); exit();
	  $this->load->library('form_validation');
	  $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
	  $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
	  $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|is_unique[customer_details.mobile]');
	  $this->form_validation->set_rules('address1', 'Address Line 1', 'trim|required');
	  $this->form_validation->set_rules('address2', 'Address Line 2', 'trim|required');
	  $this->form_validation->set_rules('city', 'City', 'trim|required');
	  $this->form_validation->set_rules('pincode', 'Pincode', 'trim|required');
	  
	  	$PancardImg    ='';
		$AdharcardImg ='';
		
		if(isset($_FILES['pancard']['name']) && $_FILES['pancard']['size'] > 0){
		
		$config['upload_path'] = './images/doc/';
		$config['overwrite'] = FALSE;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '2048';
		$IMG1 = time().$_FILES["pancard"]['name'];
		$config['file_name'] = $IMG1;
		$PancardImg = $config['file_name'];	
		$this->load->library('upload', $config);	
		$this->upload->initialize($config);	
		if ( ! $this->upload->do_upload('pancard'))	
		{
		   if ($this->form_validation->run() == FALSE)
			{
			$this->create();
			}
		}
		}

		
		if(isset($_FILES['adharcard']['name']) && $_FILES['adharcard']['size'] > 0){
		$config['upload_path'] = './images/doc/';
		$config['overwrite'] = FALSE;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '2048000';
		$IMG2 = time().$_FILES["adharcard"]['name'];
		$config['file_name'] = $IMG2;
		$AdharcardImg = $config['file_name'];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);	
		if (! $this->upload->do_upload('adharcard'))	
		{
		   if ($this->form_validation->run() == FALSE)
			{
			$this->create();
			}
		}	
		}
	  
	    if ($this->form_validation->run() == FALSE)
		{
		  // Form didn't validate
		  $this->index();
		}
		else
		{
			$now           = date('Y-m-d H:i:s');
			$dataapproved  = 0;
			
			$data = array(
                   'customerid'     => $this->SessionData['id'],
                   'firstname'      => $this->input->post('firstname'),
                   'lastname'       => $this->input->post('lastname'),
                   'mobile'         => $this->input->post('mobile'),
                   'address1'       => $this->input->post('address1'),
                   'address2'       => $this->input->post('address2'),
                   'country'        => $this->input->post('country_name'),
                   'state'          => $this->input->post('state_id'),
                   'city'           => $this->input->post('city'),
                   'pincode'        => $this->input->post('pincode'),
                   'pancard'        => $PancardImg,
                   'adharcard'      => $AdharcardImg,
                   'dataapproved'   => $dataapproved,
                   'addeddate'      => $now);
				   
		    $id = $this->dashboard_model->insert($data);
			if($id > 0){
				$this->session->set_flashdata('flash_message', 'You Succfully Upload Your KYC Form Details!.');
				redirect('Dashboard');
				
			}
			
		}
		
	}
	
	public function uploadimage() {
		$id = $this->SessionData['id'];
        if(isset($_POST['imagebase64'])){
        $data = $_POST['imagebase64'];

        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        $path = './images/profile/';
		$imagename = $id.$this->SessionData['loginid'].'.png';
        file_put_contents($path.$imagename, $data);
		$data = array(
                   'avtar'   => $imagename );
				   
        $Reulst = $this->dashboard_model->update('customer_details','customerid',$id,$data);
		
		redirect('dashboard');
        }
    }

    public function ajax_getstate() {
        $country_id = $this->input->post('data', TRUE);
        $data['result'] = $this->dashboard_model->get_state_byid($country_id);
        echo json_encode($data);
    }

	function logout()
	{
	   $this->load->helper('url');
	   $this->session->unset_userdata('logged');
	   //session_destroy();
	   redirect('login');
	}	
}
