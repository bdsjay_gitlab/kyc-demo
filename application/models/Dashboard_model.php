<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	
	public function get_country(){
      
      $sQuery="SELECT cid,cname FROM countrymst WHERE status=1";
      $query = $this->db->query($sQuery);
      return $query->result();
    }
     
    public function get_state_byid($cid) {

        $sQuery = "SELECT sid,cid,sname FROM statemst WHERE status=1 and  cid=".$cid."";
        $query = $this->db->query($sQuery);
        return $query->result();
    }
	
	public function insert($data) {

        $this->db->insert('customer_details', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    
    }
	
	public function getcustomerdetails($cust_id) {
        $lcSqlsStr = "SELECT b.firstname,b.lastname,b.avtar,b.mobile,b.address1,"
                . "b.address2,b.country,b.state,b.city,b.pincode,b.pancard,b.adharcard,c.sname,d.cname from customer_details b "
                . " left join statemst as c on c.sid=b.state left join countrymst as d on"
                . " d.cid=b.country where b.customerid=".$cust_id."";

        $query = $this->db->query($lcSqlsStr);
        $ResultSet = $query->row();
        return $ResultSet;
    }
	
	public function update($Table,$Column,$id,$data) {
	   
		$this->db->where($Column,$id);
		$this->db->update($Table, $data);
		return true;
	}
}
?>