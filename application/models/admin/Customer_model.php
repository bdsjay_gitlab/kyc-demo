<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function LoadOrderDataTable($sWhere, $sOrder, $sLimit) {
        $lcSqlsStr = "SELECT a.id,a.loginid,a.email,a.status,b.avtar,CONCAT(b.firstname,b.lastname) AS name,b.mobile from customers a left join customer_details b on a.id=b.customerid ";
        $lcSqlsStr .= "$sWhere $sOrder $sLimit";
		//echo $lcSqlsStr;
        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(a.id) as countCid  FROM customers a left join customer_details b on a.id=b.customerid $sWhere";
        $query = $this->db->query($sQuery);
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countCid'];
        return $data;
    }

    public function getcustomerdetails($cust_id) {
        $lcSqlsStr = "SELECT a.id,a.loginid,a.email,a.status,b.firstname,b.lastname,b.avtar,b.mobile,b.address1,"
                . "b.address2,b.country,b.state,b.city,b.pincode,b.pancard,b.adharcard,c.sname,d.cname from customers a left join customer_details b "
                . "on a.id=b.customerid left join statemst as c on c.sid=b.state left join countrymst as d on"
                . " d.cid=b.country where a.id=".$cust_id."";

        $query = $this->db->query($lcSqlsStr);
        $ResultSet = $query->row();
        return $ResultSet;
    }
	
	public function get_country(){
      
      $sQuery="SELECT cid,cname FROM countrymst WHERE status=1";
      $query = $this->db->query($sQuery);
      return $query->result();
    }
     
    public function get_state_byid($whercond) {

        $sQuery = "SELECT sid,cid,sname FROM statemst WHERE status=1 ".$whercond."";
        $query = $this->db->query($sQuery);
        return $query->result();
    }
	
	public function get_value($Table,$Column,$Clause)
	{
		 $LcSqlStr = "SELECT ".$Column." FROM ".$Table." WHERE ".$Clause." ";
		 $query = $this->db->query($LcSqlStr); 
	     $row = $query->row();	
	     return $row;
		 
	}
	
	public function update($Table,$Column,$id,$data) {
	   
		$this->db->where($Column,$id);
		$this->db->update($Table, $data);
		return true;
	}
	 
    public function delete($Table,$Column,$id){
		 
	 $this->db->where($Column, $id)
             ->delete($Table);
	  return true;
     }

}
?>