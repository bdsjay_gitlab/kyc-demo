<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/square/blue.css">
  <link href="<?php echo base_url();?>assets/dist/css/croppie.css" rel="stylesheet" type="text/css">

</head>
<body class="hold-transition register-page">
<div class="">
  <div class="register-logo">
    <a href="<?php echo base_url();?>dashboard"><b>KYC</b>FORM</a>
  </div>
  <h3>Hi,&nbsp;<?php echo $loginid;?></h3>&nbsp;&nbsp;<a href="<?php echo base_url();?>dashboard/logout"><b>LOGOUT</b></a>

  <div class="register-box-body">
    <?php if($this->session->flashdata('flash_message')){ ?>
	<div class="top-Sucmsg">
		<div class="msg80">
			<div class="msg94"><strong>Congratulations!</strong><?php echo $this->session->flashdata('flash_message');?></div>
			<div class="msgClose6"><img id="close" src="<?php echo base_url();?>assets/dist/img/delete.png" width="18px" height="18px" alt="" title=""></div>
		</div>
	</div>
	<?php } else { ?>
	<?php if(count($customer) > 0){?>
	<form  action="<?php echo base_url();?>dashboard/uploadimage"  method="post" enctype="multipart/form-data">
	<?php if($customer->avtar){?>
	<p class="form-control-static">
	  <?php echo $img ='<img src="'.base_url().'/images/profile/'.$customer->avtar.'" width="60">'; ?>	
	</p>
	<?php } else { ?>
	<div class="form-group has-feedback">
		<input type="file" id="upload" value="Choose a file">
		<div id="upload-demo"></div>
		<input type="hidden" id="imagebase64" name="imagebase64">
		<div class="col-xs-2">
		<button class="btn btn-primary btn-block btn-flat upload-result">Upload</button>
		</div>
	</div>
	<?php } ?>
	</br></br></br>
      <div class="form-group has-feedback">
	    <div class="col-md-6">
        <p>First Name : &nbsp;&nbsp;<?=$customer->firstname?></p>
        </div>
		<div class="col-md-6">
        <p>last Name : &nbsp;&nbsp;<?=$customer->lastname?></p>
        </div>
      </div>
	  </br></br></br>
      <div class="form-group has-feedback">
	    <div class="col-md-6">
        <p>Mobile : &nbsp;&nbsp;<?=$customer->mobile?></p>
		</div>
      </div>
	  </br></br></br>
      <div class="form-group has-feedback">
	    <div class="col-md-6">
        <p>Address Line 1 : &nbsp;&nbsp;<?=$customer->address1?></p>
        </div>
		<div class="col-md-6">
        <p>Address Line 2 : &nbsp;&nbsp;<?=$customer->address2?></p>
        </div>
      </div>
	  </br></br></br>
      <div class="form-group has-feedback">
	    <div class="col-md-6">
        <p>Country : &nbsp;&nbsp;<?=$customer->cname?></p>
        </div>
		<div class="col-md-6">
        <p>State : &nbsp;&nbsp;<?=$customer->sname?></p>
        </div>
      </div>
	  </br></br></br>
	  <div class="form-group has-feedback">
	    <div class="col-md-6">
        <p>City : &nbsp;&nbsp;<?=$customer->city?></p>
        </div>
		<div class="col-md-6">
        <p>Pincode : &nbsp;&nbsp;<?=$customer->pincode?></p>
        </div>
      </div>
	  </br></br></br>
	  <div class="form-group has-feedback">
	    <div class="col-md-6">
		Pancard Doc : &nbsp;&nbsp;
		<?php if(!empty($customer->pancard)){?>
		<p class="form-control-static">
		   <?php 
		   echo $img ='<img src="'.base_url().'/images/doc/'.$customer->pancard.'" width="60">'; 
		   ?>	
		</p>
		<?php } ?>
        </div>
		<div class="col-md-6">
		Adharcard Doc : &nbsp;&nbsp;
        <?php if(!empty($customer->adharcard)){?>
		<p class="form-control-static">
		   <?php 
		   echo $img ='<img src="'.base_url().'/images/doc/'.$customer->adharcard.'" width="60">'; 
		   ?>	
		</p>
		<?php } ?>
        </div>
      </div>
	  </br></br></br>
      
    </form>
	
	<?php } else { ?>
	<p class="login-box-msg">Fill The All Details</p>
    <form action="<?php echo base_url();?>dashboard/insert" id="KycForm" method="post" enctype="multipart/form-data" >
      <div class="form-group has-feedback">
	    <div class="col-md-6">
        <input type="text" class="form-control" name="firstname" id="firstname" placeholder="First Name" required >
        </div>
		<div class="col-md-6">
        <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last Name"  required >
        </div>
      </div>
	  </br></br></br>
      <div class="form-group has-feedback">
	    <div class="col-md-6">
        <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" required >
		</div>
      </div>
	  </br></br></br>
      <div class="form-group has-feedback">
	    <div class="col-md-6">
        <input type="text" class="form-control" name="address1" id="address1" placeholder="Address Line 1" required >
        </div>
		<div class="col-md-6">
        <input type="text" class="form-control" name="address2" id="address2" placeholder="Address Line 2"  required >
        </div>
      </div>
	  </br></br></br>
      <div class="form-group has-feedback">
	    <div class="col-md-6">
        <select name="country_name" id="country_id" class="form-control">
            <?php if(count($country)>1){
				echo '<option value="">Please Select</option>';
			}	foreach ($country as $con) { ?>
				<option value="<?= $con->cid; ?>" /><?= ucfirst(strtolower($con->cname)); ?></option>
			<?php } ?>
		</select>
        </div>
		<div class="col-md-6">
        <select name="state_id" id="state_id" class="form-control" >
			<option value="">Please Select</option>
		</select>
        </div>
      </div>
	  </br></br></br>
	  <div class="form-group has-feedback">
	    <div class="col-md-6">
        <input type="text" class="form-control" name="city" id="city" placeholder="City" required >
        </div>
		<div class="col-md-6">
        <input type="text" class="form-control" name="pincode" id="pincode" placeholder="pincode"  required >
        </div>
      </div>
	  </br></br></br>
	  <div class="form-group has-feedback">
	    <div class="col-md-6">
        <input type="file" class="form-control" name="pancard" id="pancard" placeholder="City" required >
		<span class="help-block">Max Size: 2Mb </span>
        </div>
		<div class="col-md-6">
        <input type="file" class="form-control" name="adharcard" id="adharcard" placeholder="pincode"  required >
		<span class="help-block">Max Size: 2Mb </span>
        </div>
      </div>
	  </br></br></br>
      <div class="row">
        <!--
		<div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>-->
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
	<?php }} ?>
    <!--
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div>
    -->
    
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript" ></script>
        <script type="text/javascript">
        $(document).ready(function () {
            $("#country_id").change(function () {
                    $('#state_id').empty();
                    $.ajax({
                        type: "POST",
                        data: "data=" + $(this).val(),
                        url: "<?php echo base_url(); ?>index.php/dashboard/ajax_getstate",
                        success: function (msg) {
                            if (msg != '') {

                                var Oresult = jQuery.parseJSON(msg);
                                var StateMSt = Oresult['result'];
                                var option ='<option value="">Select</option>';
                                for (var i = 0; i < StateMSt.length; i++) {
                                    option += '<option value="' + StateMSt[i]['sid'] + '">' + StateMSt[i]['sname'] + '</option>';
                                }
                                $('#state_id').append(option);
                            }
                        }
                    });
                });
               
            $('#KycForm').validate({
                focusInvalid: false, 
                ignore: "",
                rules: {
                    firstname: {
                        required: true
                    },
                    lastname: {
                        required: true
                    },
                    mobile: {
                        required: true,
                        digits: true
                    },
                    address1: {
                        required: true
                    },
                    address2: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    pincode: {
                        required: true
                    }
                    
                },
                errorPlacement: function (label, element) { // render error placement for each input type   
					$('<span class="error"></span>').insertAfter(element).append(label)
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('success-control').addClass('error-control');  
                },
                highlight: function (element) { // hightlight error inputs
					var parent = $(element).parent();
                    parent.removeClass('success-control').addClass('error-control'); 
                },
                success: function (label, element) {
                    var parent = $(element).parent('.input-with-icon');
                    parent.removeClass('error-control').addClass('success-control'); 
                },

                submitHandler: function (form) {
                        form.submit();
                }
            });	
        });
		
	$(document).ready(function() {
		
	  $('#close').click(function() {
		  $('.top-Sucmsg').css('display','none');
	  });

	});	
</script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/jquery-1.11.3.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/js/croppie.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
    var $uploadCrop;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();          
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
                $('.upload-demo').addClass('ready');
            }           
            reader.readAsDataURL(input.files[0]);
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 200,
            height: 200,
            type: 'circle'
        },
        boundary: {
            width: 300,
            height: 300
        }
    });

    $('#upload').on('change', function () { readFile(this); });
    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'original'
        }).then(function (resp) {
            $('#imagebase64').val(resp);
            $('#form').submit();
        });
    });

});
</script>