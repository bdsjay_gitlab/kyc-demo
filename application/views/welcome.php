<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="<?php echo base_url();?>welcome"><b>KYC</b>REGISTER FORM</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>
    <?php if($this->session->flashdata('flash_message')){ ?>
	<div class="top-Sucmsg">
		<div class="msg80">
			<div class="msg94"><strong>Congratulations!</strong><?php echo $this->session->flashdata('flash_message');?></div>
			<div class="msgClose6"><img id="close" src="<?php echo base_url();?>assets/dist/img/delete.png" width="18px" height="18px" alt="" title=""></div>
		</div>
	</div>
	<?php } ?>
    <form action="<?php echo base_url();?>welcome/register" id="registerInfo" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" id="username" placeholder="Username" onblur="CheckUserExits()" required >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
		<span id="errorpos" style="display:none;color:red;"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" id="email" placeholder="Email" required >
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" id="password" placeholder="Password" required >
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="confirmpassword" id="confirmpassword" placeholder="Retype password" required >
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        <!--
		<div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>-->
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <!--
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div>
    -->
    <a href="<?php echo base_url();?>login" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript" ></script>
<script type="text/javascript">
    $('#registerInfo').validate({ // initialize the plugin
        rules: {
            username: {
                required: true,
            },
			email: {
                required: true,
                email: true,
            },
			password: {
                required: true,
            },
			confirmpassword: {
                required: true,
                equalTo:'#password'
			},
        },
        messages: {
            username: "Enter UserName",
            email: {
                required: "Enter E-mail address",
                email: "Enter valid E-mail address",
            },
			password: "Enter Password",
            confirmpassword:{
             	required: "Enter Confirm Password",
            },
         },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "username") {
                $("#username").html(error);
            }
            if (element.attr("name") == "email") {
                $("#email").html(error);
            }
			if (element.attr("name") == "password") {
                $("#password").html(error);
            }
			if (element.attr("name") == "confirmpassword") {
                $("#confirmpassword").html(error);
            }
		 },
	
        highlight: function (element, errorClass, validClass) {
            if (element.type === "radio") {
                this.findByName(element.name).addClass(errorClass).removeClass(validClass);
            } else {
                $(element).addClass(errorClass).removeClass(validClass);
            }
            $(element).css('border', '1px solid #f00');
        },
        unhighlight: function (element, errorClass, validClass) {
            if (element.type === "radio") {
                this.findByName(element.name).removeClass(errorClass).addClass(validClass);
            } else {
                $(element).removeClass(errorClass).addClass(validClass);
            }
            $(element).css('border', '');
        },
        onkeyup: function( element, event ) {
			if ( event.which === 9 && this.elementValue(element) === "" ) {
				return;
			} else if ( element.name in this.submitted || element === this.lastElement ) {
				this.element(element);
			}
            $(element).css('border', '');
		},
        errorElement: "i"
    });
	
	function CheckUserExits() {
                $("#errorpos").hide();
                var logid = $("#username").val();
                $.ajax({
                    type: "POST",
                    data: "data=" + logid,
                    url: "<?php echo base_url(); ?>index.php/Welcome/ajax_checkuser",
                    success: function (msg) {
                        if (msg != '') {
                            //alert(msg);errorpos
                            $("#errorpos").html(msg).show();
                        } else {
                            // $("#committment_amount").val(msg);
                        }
                    }
             });
       }
			
	$(document).ready(function() {
		
	  $('#close').click(function() {
		  $('.top-Sucmsg').css('display','none');
	  });

	});			
</script>