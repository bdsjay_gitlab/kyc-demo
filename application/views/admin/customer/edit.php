<!DOCTYPE html>
<html>
<head>
<?php $this->load->view('admin/inc/inc_htmlhead'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<!-- BEGIN HEADER -->
<?php $this->load->view('admin/inc/inc_top_header'); ?>
<!-- END HEADER -->

<!-- BEGIN SIDEBAR -->
<?php $this->load->view('admin/inc/inc_header'); ?>
<!-- END SIDEBAR -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Customers</a></li>
        <li class="active">Customer Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Customers Edit</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <form id="customerEdit" name="customerEdit" method="post" enctype="multipart/form-data" action="<?php echo base_url();?>admin/customer/update/<?php echo $customer->id; ?>">
			    
				<?php
				$errors = validation_errors();
				if ($errors) {
					?>
					<div style="display: block;" class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						You have some form errors. Please check below.
						<?php echo $errors; ?>
					</div>
				<?php } ?>
				
                <div class="form-group">
				  <div class="col-md-6">
                  <label for="exampleInputUsername">Username</label>
                  <input type="text" class="form-control" name="username" id="exampleInputUsername" placeholder="Username" value="<?=$customer->loginid;?>" >
				  </div>
				  <div class="col-md-6">
                  <label for="exampleInputEmail">Email </label>
                  <input type="email" class="form-control" name="email" id="exampleInputEmail" placeholder="Email" value="<?=$customer->email;?>" >
				  </div>
                </div>
				
                <div class="form-group">
				  <div class="col-md-6">
                  <label for="exampleInputFirstname">First Name</label>
                  <input type="text" class="form-control" name="firstname" id="exampleInputFirstname" placeholder="First Name" value="<?=$customer->firstname;?>">
				  </div>
				  <div class="col-md-6">
                  <label for="exampleInputLastname">Last Name</label>
                  <input type="text" class="form-control" name="lastname" id="exampleInputLastname" placeholder="Last Name" value="<?=$customer->lastname;?>" >
				  </div>
                </div>
				
				<div class="form-group">
				  <div class="col-md-12">
                  <label for="exampleInputMobile">Mobile</label>
                  <input type="text" class="form-control" name="mobile" id="exampleInputMobile" placeholder="Mobile" value="<?=$customer->mobile;?>">
				  </div>
				  	</div>
				
				<div class="form-group">
				  <div class="col-md-6">
                  <label for="exampleInputAddress1">Address Line-1</label>
                  <input type="text" class="form-control" name="address1" id="exampleInputAddress1" placeholder="First Name" value="<?=$customer->address1;?>">
				  </div>
				  <div class="col-md-6">
                  <label for="exampleInputAddress2">Address Line-2</label>
                  <input type="text" class="form-control" name="address2" id="exampleInputAddress2" placeholder="Last Name" value="<?=$customer->address2;?>" >
				  </div>
                </div>
				
                <div class="form-group">
				  <div class="col-md-6">
                  <label for="exampleInputAddress2">Country</label>
                  <select class="form-control" name="country" id="country">
					<?php if (count($country) > 1) { ?>
					<option value="" >Please Select</option>
					<?php	} foreach ($country as $con) { ?>
						<option value="<?= $con->cid; ?>" <?= ($con->cid == $customer->country ? 'Selected' : ''); ?> ><?= ucfirst(strtolower($con->cname)); ?></option>
                    <?php } ?>
				  </select>
				  </div>
				  <div class="col-md-6">
                  <label for="exampleInputState">State</label>
                  <select name="state_id" id="state_id" class="form-control">
				  <option value="" >Please Select</option>
					<?php 	foreach ($state as $sta) { ?>
						<option value="<?= $sta->sid; ?>" <?= ($sta->sid == $customer->state ? 'Selected' : ''); ?> ><?= ucfirst(strtolower($sta->sname)); ?></option>
                    <?php } ?>
				  </select>
				  </div>
                </div>
				
				<div class="form-group">
				  <div class="col-md-6">
                  <label for="exampleInputCity">City</label>
                  <input type="text" class="form-control" name="city" id="exampleInputCity" placeholder="City" value="<?=$customer->city;?>">
				  </div>
				  <div class="col-md-6">
                  <label for="exampleInputPincode">Pincode</label>
                  <input type="text" class="form-control" name="pincode" id="exampleInputPincode" placeholder="Pincode" value="<?=$customer->pincode;?>" >
				  </div>
                </div>
				
				<div class="form-group">
				  <div class="col-md-6">
                  <label for="exampleInputPancard">Pancard</label>
                  <?php if(!empty($customer->pancard)){?>
					<p class="form-control-static">
					   <?php 
					   echo $img ='<img src="'.base_url().'/images/doc/'.$customer->pancard.'" width="60">'; 
					   ?>	
					</p>
					<?php } ?>
					<br/>
					<input name="panimage_old" type="hidden" class="input" id="panimage_old" value="<?php echo $customer->pancard; ?>">
					<input type="file" name="panimage" value="<?php echo $customer->pancard; ?>">
					<span class="help-block">Max Size: 2Mb </span>
				  </div>
				  <div class="col-md-6">
                  <label for="exampleInputAdharcard">AdharCard</label>
                  
					<?php if(!empty($customer->adharcard)){?>
					<p class="form-control-static">
					   <?php 
					   echo $img ='<img src="'.base_url().'/images/doc/'.$customer->adharcard.'" width="60">'; 
					   ?>	
					</p>
					<?php } ?>
					<br/>
					<input name="adharimage_old" type="hidden" class="input" id="adharimage_old" value="<?php echo $customer->adharcard; ?>">
					<input type="file" name="adharimage" value="<?php echo $customer->adharcard; ?>">
					<span class="help-block">Max Size: 2Mb </span>
				  </div>
                </div>
				<div class="form-group" >
				<div class="form-actions right">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="<?php echo site_url("admin/customer"); ?>" class="btn btn default">Back</a>
				</div>
				</div>
             </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('admin/inc/inc_footer_script'); ?>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript" ></script>
<script type="text/javascript">
            $("#country").change(function () {

                $.ajax({
                    type: "POST",
                    data: "data=" + $(this).val(),
                    url: "<?php echo base_url(); ?>index.php/admin/customer/ajax_getstate",
                    success: function (msg) {
                        if (msg != '') {

                            var Oresult = jQuery.parseJSON(msg);
                            var StateMSt = Oresult['result'];
                            var option;
                            for (var i = 0; i < StateMSt.length; i++) {
                                option += '<option value="' + StateMSt[i]['sid'] + '">' + StateMSt[i]['sname'] + '</option>';
                            }
                            $('#state_id').append(option);
                            $("#state_id").trigger("change");
                        } else {
                            // $("#committment_amount").val(msg);
                        }
                    }
                });
            });
			
        $(document).ready(function () {

              

                var form3 = $('#customerEdit');
                var error3 = $('.alert-danger', form3);
                var success3 = $('.alert-success', form3);

                form3.validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "", // validate all fields including form hidden input
                    rules: {
                        username: {
                            required: true
                        },
                        email: {
                            required: true
                        },
						firstname: {
                            required: true
                        },
                        lastname: {
                            required: true
                        },
                        mobile: {
                            required: true,
							checkcontact: true,
                            maxlength: 10
                        },
                        city: {
                            required: true

                        }

                    },
                    messages: {// custom messages for radio buttons and checkboxes

                    },
                    errorPlacement: function (error, element) { // render error placement for each input type
                        if (element.parent(".input-group").size() > 0) {
                            error.insertAfter(element.parent(".input-group"));
                        } else if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else if (element.parents('.radio-list').size() > 0) {
                            error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                        } else if (element.parents('.radio-inline').size() > 0) {
                            error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                        } else if (element.parents('.checkbox-list').size() > 0) {
                            error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                        } else if (element.parents('.checkbox-inline').size() > 0) {
                            error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                        } else {
                            error.insertAfter(element); // for other inputs, just perform default behavior
                        }
                    },
                    invalidHandler: function (event, validator) { //display error alert on form submit   
                        success3.hide();
                        error3.show();
                        Metronic.scrollTo(error3, -200);
                    },
                    highlight: function (element) { // hightlight error inputs
                        $(element)
                                .closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element)
                                .closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function (label) {
                        label
                                .closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    submitHandler: function (form) {
                        success3.show();
                        error3.hide();
                        form[0].submit(); // submit the form
                    }

                });
                $.validator.addMethod('checkcontact', function (value, element) {
                    return this.optional(element) || /^[ 0-9-]*$/.test(value);
                }, "Please enter a valid phone number");


            });   
		</script>	
</body>
</html>