<!DOCTYPE html>
<html>
<head>
<?php $this->load->view('admin/inc/inc_htmlhead'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/dist/css/jquery-confirm.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/dist/css/magnific-popup.css"/>
<style> 
.row{margin-bottom:5px;} 
</style>  
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<!-- BEGIN HEADER -->
<?php $this->load->view('admin/inc/inc_top_header'); ?>
<!-- END HEADER -->

<!-- BEGIN SIDEBAR -->
<?php $this->load->view('admin/inc/inc_header'); ?>
<!-- END SIDEBAR -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Customer</a></li>
        <li class="active">Customers List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
	    <div class="row" style="margin-top:15px;">
			<div class="col-md-12">
                <div class="panel-group accordion" id="accordion3">
                    <div class="panel panel-default">


						<div class="panel-heading">
							<h4 class="panel-title">
							   <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
								<!--<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2">-->
									Advanced Search </a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse">
						
							<div class="panel-body" style="height:200px; overflow-y:auto;">
								<form id="Filter_Frm" name="Filter_Frm" method="post">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Search By&nbsp;</label>
												   <div class="col-md-9">
												<select name="SearchBy" id="SearchBy" class="form-control">
														<option value="0">All</option>
														<option value="1">First Name</option>
													<option value="2">Email</option>
													<option value="3">Mobile No</option>
												</select>
												  </div>                                                      
											</div>
												</div>
											<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Keyword</label>
												<div class="col-md-9">
												   <input id="input-keyword" class="form-control" type="text" placeholder="Keyword" name="input-keyword">   
												</div>
											</div>
										</div>
												
										   
											
											<!--/span-->
										</div>
										<div class="row">
									<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Status&nbsp;</label>
													<div class="col-md-9">
														<select name="status" id="status" class="form-control" >
															<option value="">All</option>
															<option value="1">Active</option>
															<option value="0">Inactive</option>
														</select>
													</div>
												</div>
											</div>	
									
										</div>
										
										<div class="clearfix margin-top-20"></div>
										<div class="row">
											<div class="col-md-6">

											</div>
											<div class="col-md-6">

												<div class="col-md-offset-3 col-md-9">
													<button type="button" class="btn btn-primary btn-sm btn-small" onclick="setFilter()">Search</button>
													<button type="button" class="btn  btn-sm btn-small" onclick="ResetFilter()">Reset</button>
												</div>
											</div>
										</div>
										<div class="clearfix margin-top-20"></div>                                 
									</form> 
								<div class="clearfix margin-top-20"></div>

							</div>
						</div>


                    </div>
                </div>
            </div>
        </div>
					
      <div class="row">
        <div class="col-xs-12">
            <div class="row">
				<div class="col-md-12">
					<button type="Active" class="btn btn-primary" name="Active" onclick="ValidCheckbox(1)">Active</button>
					<button type="Deactive" class="btn btn-primary" name="Inactive" onclick="ValidCheckbox(2)">Deactive</button>
					<button type="Delete" class="btn btn-primary" name="Delete" onclick="ValidCheckbox(3)" >Delete</button>
				</div>
			</div>
			<div class="clearfix margin-bottom-20"></div>
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Customers List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <form id="custlist_frm" name="custlist_frm" method="post">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
					<th class="table-checkbox"><input type="checkbox" name="check" id="check" class="all"></th>
					<th  width="10%">ID #</th>
					<th>Username</th>
					<th>Avtar</th>
					<th>Name</th>
					<th>Email</th>
					<th>Mobile</th>
					<th>Status </th>
					<th>Actions</th>
				</tr>
                </thead>
                <tbody>
                </tbody>
               </table>
			   <input type="hidden" id ="Action" name="Action">
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('admin/inc/inc_footer_script'); ?>
<script type="text/javascript"  src="<?php echo base_url();?>assets/dist/js/jquery-confirm.js"></script>
<script type="text/javascript"  src="<?php echo base_url();?>assets/dist/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript"  src="<?php echo base_url();?>assets/dist/js/jquery.mixitup.js"></script>
<script type="text/javascript"  src="<?php echo base_url();?>assets/dist/js/gallery.js"></script>
<!-- page script -->

        <script type="text/javascript">
		    var frm = document.Filter_Frm;
            var frm1 = document.custlist_frm;

            jQuery(document).ready(function () {
                DatatableRefresh();
                $('.all').click(function () {
					if ($(this).is(':checked')) {
						$("input[name='id[]']").prop('checked', true);
                    } else {
                        $("input[name='id[]']").prop('checked', false);
                    }
                });
            });
			function ResetFilter() {
                frm.reset();
				DatatableRefresh();
            }
            function setFilter() {
                DatatableRefresh();
            }

            function DatatableRefresh() {
				
                $('#example1').dataTable({
				    "columnDefs": [ {
                           orderable: false,
                           className: 'select-checkbox',
                           targets:   0
						} ],
                    "select": {
							style:    'os',
							selector: 'td:first-child'
						},
                    "scrollY": "600",
                    "deferRender": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "aaSorting": [[1, "desc"]],
                    "lengthMenu": [
                        [50, 100, 500, -1],
                        [50, 100, 500, "All"] // change per page values here
                    ],
                    "bScrollCollapse": true,
                    "bAutoWidth": true,
                    "sPaginationType": "full_numbers",
                    "bDestroy": true,
                    "sAjaxSource": "<?php echo base_url().'index.php/admin/customer/DataTableRefresh'; ?>",
                    "fnServerData": function (sSource, aoData, fnCallback) {
						   aoData.push({name: 'SearchBy', value:$('#SearchBy').val()});
						   aoData.push({name: 'status', value: $('#status').val()});
						   aoData.push({name: 'input-keyword', value:$('#input-keyword').val()});
                           $.getJSON(sSource, aoData, function (json) {
                            fnCallback(json);
                        });
                    },
                    "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                        return nRow;
                    },
					"fnFooterCallback": function (nRow, aData) {
                    }
                });

            }
			
	function LoadDeleteDialog(Name,Id){
                $.confirm({
                    title: 'Confirm!',
                    content: " are you sure you want to delete '"+Name+"' ? ",
                    buttons: {
                        confirm:{
                        text: 'Confirm',
                        btnClass: 'btn-danger',
                        keys: ['enter', 'shift'],
                        action: function(){
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url().'index.php/admin/customer/remove/'; ?>",
                                data: {deleteid:Id},
                                success: function (msg) {
                                    ShowAlret(msg);
                                    $('#modelClose').click();
                                    DatatableRefresh();
                                }
                            });
                        }
                    },
                    cancel: function () {
                         this.onClose();
                    }
                    }
                });
            }	

    function ValidCheckbox(opt){
        var Check = getCheckCount();
        if (Check > 0){
            if( opt == 1 ){
               LoadConfirmDialog("Confirm Active Customer(s) Status ?",opt);  
            }
            else if( opt == 2 ){
                LoadConfirmDialog("Confirm InActive Customer(s) Status ?",opt);
            }
            else if( opt == 3 ){
                LoadConfirmDialog("Delete Selected Customer(s) ?",opt);
            }   
        }else{
            ShowAlret("Please select Checkbox.",'error');
        }
    }
    function LoadConfirmDialog(content,opt){
        $.confirm({
            title: 'Confirm!',
            content: content,
            buttons: {
                confirm:{
                text: 'Confirm',
                btnClass: 'btn-primary',
                keys: ['enter', 'shift'],
                action: function(){
                    $.ajax({
                    type: "POST",
                    data: $('#custlist_frm').serialize(),
                    url: "<?php echo base_url().'admin/customer/Actionongrid/'; ?>"+opt,
                    success: function (msg) {
                        if (msg != '') {
                            ShowAlret(msg);
                            DatatableRefresh();
                        }
                    }
                });
                }
            },
            cancel: function () {
                 this.onClose();
            }
            }
        });
    }			
			
	function getCheckCount()
    {
        var x = 0;
        for (var i = 0; i < frm1.elements.length; i++)
        {
            if (frm1.elements[i].checked == true)
            {
                x++;
            }
        }
        return x;
    }
	
</script>
</body>
</html>