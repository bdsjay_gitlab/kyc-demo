<!DOCTYPE html>
<html>
<head>
<?php $this->load->view('admin/inc/inc_htmlhead'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<!-- BEGIN HEADER -->
<?php $this->load->view('admin/inc/inc_top_header'); ?>
<!-- END HEADER -->

<!-- BEGIN SIDEBAR -->
<?php $this->load->view('admin/inc/inc_header'); ?>
<!-- END SIDEBAR -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Customers Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			 <form id="customerEdit" name="customerEdit" method="post" enctype="multipart/form-data" >
			    
				<?php
				$errors = validation_errors();
				if ($errors) {
					?>
					<div style="display: block;" class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						You have some form errors. Please check below.
						<?php echo $errors; ?>
					</div>
				<?php } ?>
				
                <div class="form-group">
				  <div class="col-md-6">
                  <label for="exampleInputUsername">Username</label>
                  <p><?=$customer->loginid;?></p>
				  </div>
				  <div class="col-md-6">
                  <label for="exampleInputEmail">Email </label>
                  <p><?=$customer->email;?></p>
				  </div>
                </div>
				
                <div class="form-group">
				  <div class="col-md-6">
                  <label for="exampleInputFirstname">First Name</label>
                  <p><?=$customer->firstname;?></p>
				  </div>
				  <div class="col-md-6">
                  <label for="exampleInputLastname">Last Name</label>
                  <p><?=$customer->lastname;?></p>
				  </div>
                </div>
				
				<div class="form-group">
				  <div class="col-md-12">
                  <label for="exampleInputMobile">Mobile</label>
                  <p><?=$customer->mobile;?></p>
				  </div>
				  	</div>
				
				<div class="form-group">
				  <div class="col-md-6">
                  <label for="exampleInputAddress1">Address Line-1</label>
                  <p><?=$customer->address1;?></p>
				  </div>
				  <div class="col-md-6">
                  <label for="exampleInputAddress2">Address Line-2</label>
                  <p><?=$customer->address2;?></p>
				  </div>
                </div>
				
                <div class="form-group">
				  <div class="col-md-6">
                  <label for="exampleInputAddress2">Country</label>
                  <p><?=$customer->cname;?></p>
				  </div>
				  <div class="col-md-6">
                  <label for="exampleInputState">State</label>
                  <p><?=$customer->sname;?></p>
				  </div>
                </div>
				
				<div class="form-group">
				  <div class="col-md-6">
                  <label for="exampleInputCity">City</label>
                  <p><?=$customer->city;?></p>
				  </div>
				  <div class="col-md-6">
                  <label for="exampleInputPincode">Pincode</label>
                  <p><?=$customer->pincode;?></p>
				  </div>
                </div>
				
				<div class="form-group">
				  <div class="col-md-6">
                  <label for="exampleInputPancard">Pancard</label>
                  <?php if(!empty($customer->pancard)){?>
					<p class="form-control-static">
					   <?php 
					   echo $img ='<img src="'.base_url().'/images/doc/'.$customer->pancard.'" width="60">'; 
					   ?>	
					</p>
					<?php } ?>
					<br/>
				  </div>
				  <div class="col-md-6">
                  <label for="exampleInputAdharcard">AdharCard</label>
                  
					<?php if(!empty($customer->pancard)){?>
					<p class="form-control-static">
					   <?php 
					   echo $img ='<img src="'.base_url().'/images/doc/'.$customer->adharcard.'" width="60">'; 
					   ?>	
					</p>
					<?php } ?>
					<br/>
				 </div>
                </div>
				<div class="form-group" >
				<div class="form-actions right">
					<a href="<?php echo site_url("admin/customer"); ?>" class="btn btn primary">Back</a>
				</div>
				</div>
             </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>


  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?php $this->load->view('admin/inc/inc_footer_script'); ?>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript" ></script>
<script type="text/javascript">
            $("#country").change(function () {

                $.ajax({
                    type: "POST",
                    data: "data=" + $(this).val(),
                    url: "<?php echo base_url(); ?>index.php/admin/customer/ajax_getstate",
                    success: function (msg) {
                        if (msg != '') {

                            var Oresult = jQuery.parseJSON(msg);
                            var StateMSt = Oresult['result'];
                            var option;
                            for (var i = 0; i < StateMSt.length; i++) {
                                option += '<option value="' + StateMSt[i]['sid'] + '">' + StateMSt[i]['sname'] + '</option>';
                            }
                            $('#state_id').append(option);
                            $("#state_id").trigger("change");
                        } else {
                            // $("#committment_amount").val(msg);
                        }
                    }
                });
            });
			
        $(document).ready(function () {

              

                var form3 = $('#customerEdit');
                var error3 = $('.alert-danger', form3);
                var success3 = $('.alert-success', form3);

                form3.validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "", // validate all fields including form hidden input
                    rules: {
                        username: {
                            required: true
                        },
                        email: {
                            required: true
                        },
						firstname: {
                            required: true
                        },
                        lastname: {
                            required: true
                        },
                        mobile: {
                            required: true,
							checkcontact: true,
                            maxlength: 10
                        },
                        city: {
                            required: true

                        }

                    },
                    messages: {// custom messages for radio buttons and checkboxes

                    },
                    errorPlacement: function (error, element) { // render error placement for each input type
                        if (element.parent(".input-group").size() > 0) {
                            error.insertAfter(element.parent(".input-group"));
                        } else if (element.attr("data-error-container")) {
                            error.appendTo(element.attr("data-error-container"));
                        } else if (element.parents('.radio-list').size() > 0) {
                            error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                        } else if (element.parents('.radio-inline').size() > 0) {
                            error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                        } else if (element.parents('.checkbox-list').size() > 0) {
                            error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                        } else if (element.parents('.checkbox-inline').size() > 0) {
                            error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                        } else {
                            error.insertAfter(element); // for other inputs, just perform default behavior
                        }
                    },
                    invalidHandler: function (event, validator) { //display error alert on form submit   
                        success3.hide();
                        error3.show();
                        Metronic.scrollTo(error3, -200);
                    },
                    highlight: function (element) { // hightlight error inputs
                        $(element)
                                .closest('.form-group').addClass('has-error'); // set error class to the control group
                    },
                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element)
                                .closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },
                    success: function (label) {
                        label
                                .closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },
                    submitHandler: function (form) {
                        success3.show();
                        error3.hide();
                        form[0].submit(); // submit the form
                    }

                });
                $.validator.addMethod('checkcontact', function (value, element) {
                    return this.optional(element) || /^[ 0-9-]*$/.test(value);
                }, "Please enter a valid phone number");


            });   
		</script>	
</body>
</html>