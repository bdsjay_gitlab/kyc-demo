-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2017 at 09:25 AM
-- Server version: 5.5.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kycdemo`
--

-- --------------------------------------------------------

--
-- Table structure for table `countrymst`
--

CREATE TABLE `countrymst` (
  `cid` int(11) NOT NULL,
  `cname` varchar(50) DEFAULT NULL,
  `ciso2` varchar(5) DEFAULT NULL,
  `ciso3` varchar(5) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `addeddate` datetime NOT NULL,
  `addedby` int(11) NOT NULL,
  `modifieddate` datetime NOT NULL,
  `modifiedby` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countrymst`
--

INSERT INTO `countrymst` (`cid`, `cname`, `ciso2`, `ciso3`, `status`, `addeddate`, `addedby`, `modifieddate`, `modifiedby`) VALUES
(1, 'India', 'IN', 'IND', 1, '0000-00-00 00:00:00', 0, '2014-06-11 11:26:21', 1),
(2, 'Australia', 'AU', 'AUS', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `loginid` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `addeddate` datetime NOT NULL,
  `addedby` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `loginid`, `password`, `email`, `status`, `addeddate`, `addedby`, `deleted`) VALUES
(1, 'admin', '123456', 'santosh@gmail.com', 1, '2017-03-02 13:49:11', 0, 0),
(2, 'mwadmin', '123456', 'mwadmin@gmail.com', 1, '2017-03-04 07:54:45', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_details`
--

CREATE TABLE `customer_details` (
  `id` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `avtar` text NOT NULL,
  `mobile` varchar(25) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `country` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `city` varchar(50) NOT NULL,
  `pincode` varchar(25) NOT NULL,
  `pancard` text NOT NULL,
  `adharcard` text NOT NULL,
  `dataapproved` int(11) NOT NULL,
  `addeddate` datetime NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_details`
--

INSERT INTO `customer_details` (`id`, `customerid`, `firstname`, `lastname`, `avtar`, `mobile`, `address1`, `address2`, `country`, `state`, `city`, `pincode`, `pancard`, `adharcard`, `dataapproved`, `addeddate`, `deleted`) VALUES
(1, 1, 'Sam', 'Dhaml', '', '1234567890', 'Nellkanth', 'Vidyavihar Staion', 1, 19, 'Mumbai', '400085', '1488541814Chrysanthemum.jpg', '1488541814Desert.jpg', 0, '2017-03-03 00:00:00', 0),
(4, 2, 'Virat', 'Kohli', '2mwadmin.png', '9988776655', 'Asalpha', 'Ghatkopar', 1, 19, 'Mumbai', '400084', '1488611195Koala.jpg', '1488611196Lighthouse.jpg', 0, '2017-03-04 08:06:36', 0);

-- --------------------------------------------------------

--
-- Table structure for table `statemst`
--

CREATE TABLE `statemst` (
  `sid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `sname` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `addeddate` datetime NOT NULL,
  `addedby` int(11) NOT NULL,
  `modifieddate` datetime NOT NULL,
  `modifiedby` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statemst`
--

INSERT INTO `statemst` (`sid`, `cid`, `sname`, `code`, `status`, `addeddate`, `addedby`, `modifieddate`, `modifiedby`) VALUES
(1, 1, 'Andaman and Nicobar Islands', 'AN', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(2, 1, 'Andhra Pradesh', 'AP', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(3, 1, 'Arunachal Pradesh', 'AR', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(4, 1, 'Assam', 'AS', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(5, 1, 'Bihar', 'BI', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(6, 1, 'Chandigarh', 'CH', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(7, 1, 'Dadra and Nagar Haveli', 'DA', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(8, 1, 'Daman and Diu', 'DM', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(9, 1, 'Delhi', 'DE', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(10, 1, 'Goa', 'GO', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(11, 1, 'Gujarat', 'GU', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(12, 1, 'Haryana', 'HA', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(13, 1, 'Himachal Pradesh', 'HP', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(14, 1, 'Jammu and Kashmir', 'JA', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(15, 1, 'Karnataka', 'KA', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(16, 1, 'Kerala', 'KE', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(17, 1, 'Lakshadweep Islands', 'LI', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(18, 1, 'Madhya Pradesh', 'MP', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(19, 1, 'Maharashtra', 'MA', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(20, 1, 'Manipur', 'MN', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(21, 1, 'Meghalaya', 'ME', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(22, 1, 'Mizoram', 'MI', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(23, 1, 'Nagaland', 'NA', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(24, 1, 'Orissa', 'OR', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(25, 1, 'Pondicherry', 'PO', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(26, 1, 'Punjab', 'PU', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(27, 1, 'Rajasthan', 'RA', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(28, 1, 'Sikkim', 'SI', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(29, 1, 'Tamil Nadu', 'TN', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(30, 1, 'Tripura', 'TR', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(31, 1, 'Uttar Pradesh', 'UP', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0),
(32, 1, 'West Bengal', 'WB', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `email_id` varchar(100) NOT NULL,
  `first_name` varchar(60) DEFAULT NULL,
  `last_name` varchar(60) DEFAULT NULL,
  `mobile` varchar(14) DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `currentlogin` datetime DEFAULT NULL,
  `lastIP` varchar(20) DEFAULT NULL,
  `currentIP` varchar(20) DEFAULT NULL,
  `logincount` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `addeddate` date DEFAULT NULL,
  `addedby` int(11) DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `username`, `password`, `email_id`, `first_name`, `last_name`, `mobile`, `lastlogin`, `currentlogin`, `lastIP`, `currentIP`, `logincount`, `status`, `addeddate`, `addedby`, `modifieddate`, `modifiedby`, `deleted`) VALUES
(1, 'admin', 'mwadmin', 'admin@kycdemo.in', 'Kyc', 'Demo', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-03-02', NULL, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countrymst`
--
ALTER TABLE `countrymst`
  ADD PRIMARY KEY (`cid`),
  ADD KEY `cid` (`cid`),
  ADD KEY `cid_2` (`cid`),
  ADD KEY `cid_3` (`cid`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_details`
--
ALTER TABLE `customer_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mobile` (`mobile`);

--
-- Indexes for table `statemst`
--
ALTER TABLE `statemst`
  ADD PRIMARY KEY (`sid`),
  ADD KEY `sid` (`cid`,`sid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countrymst`
--
ALTER TABLE `countrymst`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customer_details`
--
ALTER TABLE `customer_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `statemst`
--
ALTER TABLE `statemst`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
